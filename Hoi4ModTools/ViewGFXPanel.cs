﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Hoi4_Mod_Tools {
    class ViewGFXPanel : MonoGame.Forms.Controls.MonoGameControl {

        Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch;
        public int scrollBarX = 0;
        public int scrollBarY = 0;

        public int maxScrollBarX = 0;
        public int maxScrollBarY = 0;

        public Microsoft.Xna.Framework.Graphics.SpriteFont arialfont12;
        protected override void Initialize() {
            base.Initialize();
            this.spriteBatch = new Microsoft.Xna.Framework.Graphics.SpriteBatch(this.GraphicsDevice);
			
            arialfont12 = Editor.Content.Load<Microsoft.Xna.Framework.Graphics.SpriteFont>("arialFont12");
        }

        protected override void Update(GameTime gameTime) {
            base.Update(gameTime);

            if (this.Parent.GetType() == typeof(gfxViewWindow)) {
                gfxViewWindow parentWindow = (gfxViewWindow)this.Parent;
                parentWindow.updateScrollbar();
            }
        }

        protected override void Draw() {
            base.Draw();
            gfxViewWindow gfxParent = null;
            if (this.Parent.GetType() == typeof(gfxViewWindow)) {
                gfxParent = (gfxViewWindow)this.Parent;
            }
            int c = 0;
            int v = 0;
            spriteBatch.Begin(Microsoft.Xna.Framework.Graphics.SpriteSortMode.Deferred, Microsoft.Xna.Framework.Graphics.BlendState.NonPremultiplied);
            if (gfxParent != null) {
                foreach (var item in gfxParent.listBox1.SelectedItems) {
                    if (item.GetType() == typeof(GFXSprite)) {
                        GFXSprite gfxSprite = (GFXSprite)item;
                        if (!gfxSprite.IsLoaded()) { continue; }
                        spriteBatch.Draw(gfxSprite.Texture,
                            new Rectangle(c * 100 - this.scrollBarX, v * 100 - this.scrollBarY,
                            MathHelper.Min(100, gfxSprite.Texture.Width), MathHelper.Min(100, gfxSprite.Texture.Height)), Color.White);
                    }
                }
            }
            maxScrollBarX = c * 100;
            maxScrollBarY = v * 100;
            spriteBatch.End();
        }
    }
}

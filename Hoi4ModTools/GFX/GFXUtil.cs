﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Hoi4_Mod_Tools {
    static class GFXUtil {
        public static Bitmap DDSToBitmap(DDSLib.Dds.DdsFile ddsFile) {
            try {
                Bitmap toReturn;
                MemoryStream outStream = new MemoryStream();
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(ddsFile.BitmapSource));
                enc.Save(outStream);
                toReturn = new System.Drawing.Bitmap(outStream);
                return toReturn;
            }
            catch (Exception e) {
                Console.WriteLine("Error occured in DDSToBitmap");
                return null;
            }

        }

        public static Bitmap DDSToPNGBitmap(DDSLib.Dds.DdsFile ddsFile) {
            try {
                Bitmap toReturn;
                MemoryStream outStream = new MemoryStream();
                BitmapEncoder enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(ddsFile.BitmapSource));
                enc.Save(outStream);
                toReturn = new System.Drawing.Bitmap(outStream);
                return toReturn;
            }
            catch (Exception e) {
                Console.WriteLine("Error occured in DDSToBitmap");
                return null;
            }

        }
    }
}

﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Hoi4_Mod_Tools {
    /// <summary>
    /// Used to store GFX data.
    /// </summary>
    class GFXSprite {
        public static Dictionary<String, GFXSprite> GFXSprites = new Dictionary<string, GFXSprite>();
        public static bool AddGFXSprite(GFXSprite sprite) {
            if (GFXSprites.ContainsKey(sprite.Name)) { return false; }
            GFXSprites.Add(sprite.Name, sprite);
            try {
                OnSpriteAdd.Invoke(sprite);
            }
            catch (Exception e) { }
            return true;
        }

        public delegate void OnSpriteEventHandler(GFXSprite spriteAdded);
        public static event OnSpriteEventHandler OnSpriteAdd;

        public String Name { get; set; }
        public String TextureFile { get; set; }
        public String FullTexturePath { get { return UpperPath + TextureFile; } private set { } }
        public String UpperPath { get; set; }
        public Texture2D Texture { get; set; }
        public bool HasFailedToLoad { get; private set; }
        public Dictionary<String, Object> MiscData = new Dictionary<string, object>();
        
        public GFXSprite() {

        }

        public GFXSprite (String name, String fileTexture) {
            this.Name = name;
            this.TextureFile = fileTexture;
        }

        public Boolean IsLoaded() {
            return Texture != null;
        }

        /// <summary>
        /// Loads the texture specified by the internal TexturePath.
        /// </summary>
        /// <param name="graphics">The graphics device used to load this into.</param>
        /// <param name="Reload">If the texture is already loaded, do we reload?</param>
        public void LoadTexture(GraphicsDevice graphics, bool Reload = false) {
            if (IsLoaded() && !Reload) {
                return; 
            }
            try {
                if (!FullTexturePath.EndsWith(".dds")) {
                    using (FileStream fRead = System.IO.File.Open(FullTexturePath, System.IO.FileMode.Open)) {
                        this.Texture = Texture2D.FromStream(graphics, fRead);
                    }
                }
                else {
                    try {
                        DDSLib.Dds.DdsFile ddsFile = new DDSLib.Dds.DdsFile(FullTexturePath);
                        Bitmap bitmap = GFXUtil.DDSToPNGBitmap(ddsFile);
                        using (MemoryStream bStream = new MemoryStream()) {
                            bitmap.Save(bStream, System.Drawing.Imaging.ImageFormat.Png);
                            bStream.Seek(0, SeekOrigin.Begin);
                            this.Texture = Texture2D.FromStream(graphics, bStream);
                        }

                        bitmap.Dispose();
                    }
                    catch (FormatException e) {
                        using (FileStream fRead = System.IO.File.Open(FullTexturePath, System.IO.FileMode.Open)) {
                            this.Texture = Texture2D.FromStream(graphics, fRead);
                        }


                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                Console.WriteLine(this.FullTexturePath);
                this.HasFailedToLoad = true;
            }
        }

        public void ConstructFilePath() {

            TextureFile = TextureFile.TrimStart('"');
            TextureFile = TextureFile.TrimEnd('"');
            TextureFile = TextureFile.Replace('/', '\\');
            TextureFile = '\\' + TextureFile;

            DirectoryInfo dir = Directory.GetParent(this.UpperPath);
            this.UpperPath = dir.FullName;

            if (dir.FullName.EndsWith("interface")) {
                dir = Directory.GetParent(dir.FullName);
                this.UpperPath = dir.FullName;
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hoi4_Mod_Tools {
    class GFXParser {

        public static GFXParserResult ParseGFXFIle(String file, bool loadToDict = false) {
            Token rootToken;
            GFXParserResult parseResult = new GFXParserResult();
            List<GFXSprite> gfxSpriteTypeTokens = new List<GFXSprite>();
            try {
                rootToken = Tokenizer.TokenizeFile(file);
            }
            catch (Exception e) {
                parseResult.FailureReason = "Failed to tokenize file: " + file + " due to the following: " + Environment.NewLine;
                parseResult.FailureReason += e.Message;
                parseResult.Success = false;
                return parseResult;
            }


            Token currentToken = rootToken;
            try {
                traverseTokenTree(rootToken, gfxSpriteTypeTokens, null);
            }
            catch (Exception e) {
                parseResult.FailureReason = "Failed to recursively construct GFXSprites from tokens." + Environment.NewLine;
                parseResult.FailureReason += e.Message;
                parseResult.Success = false;
                return parseResult;
            }

            parseResult.Result = gfxSpriteTypeTokens;
            parseResult.Success = true;

            if (loadToDict) {
                foreach (GFXSprite sprite in gfxSpriteTypeTokens) {
                    GFXSprite.AddGFXSprite(sprite);
                }
            }
            return parseResult;

        }

        private static void traverseTokenTree(Token token, List<GFXSprite> spriteList, GFXSprite constructing) {
            switch(token.tokenType) {
                case TokenType.Comment: return;
                case TokenType.SingleValue: return;
                case TokenType.Unknown: return;
            }
            if (token.Key == null) { return; }
            switch(token.Key.ToLower()) {
                case "spritetype":
                    constructing = new GFXSprite("", "");
                    constructing.UpperPath = token.FileFrom;
                    spriteList.Add(constructing);
                    break;
                case "name":
                    if (constructing != null) {
                        constructing.Name = token.Value;
                    }
                    break;
                case "texturefile":
                    if (constructing != null) {
                        constructing.TextureFile = token.Value;
                    }
                    break;
                case "legacy_lazy_load":
                    if (token.Value != null && constructing != null) {
                        if (token.Value.ToLower() == "yes") { constructing.MiscData["legacy_lazy_load"] = true; }
                        else { constructing.MiscData["legacy_lazy_load"] = false; }
                    }
                    break;
            }
            
            foreach (Token t in token.Children) {
                traverseTokenTree(t, spriteList, constructing);
            }

        }

    }

    /// <summary>
    /// An object to be returned when constructing GFXSprites.
    /// </summary>
    struct GFXParserResult {
        /// <summary>
        /// Was the parsing of the file successful in constructing a list of GFXSprites
        /// </summary>
        public Boolean Success;

        /// <summary>
        /// The reason for failure if it was not successful.
        /// </summary>
        public String FailureReason;

        /// <summary>
        /// The resulting list of GFXSprites constructed.
        /// </summary>
        public List<GFXSprite> Result;
    }
}

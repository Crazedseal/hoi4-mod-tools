﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hoi4_Mod_Tools {
	enum ImportFileType {
		Focus, GFX, Unknown, Event, Decision, Other
	}

	class ImportFile {
		public String Name { get; set; }
		public String Path { get; set; }
		public Token RootToken;
		public ImportFileType ImportFileType = ImportFileType.Unknown;

		public ImportFile(String name, String path) {
			this.Name = name;
			this.Path = path;
		}

		public void FindFileType() {
			if (RootToken == null) { return; }
			
			foreach (Token child in RootToken.Children) {
				if (child.tokenType == TokenType.KeyCollection) {
					switch (child.Key.ToLower()) {
						case "focus_tree":
							this.ImportFileType = ImportFileType.Focus;
							break;
						case "shared_focus":
							this.ImportFileType = ImportFileType.Focus;
							break;
						case "country_event":
							this.ImportFileType = ImportFileType.Event;
							break;
						case "state_event":
							this.ImportFileType = ImportFileType.Event;
							break;
						case "leader_event":
							this.ImportFileType = ImportFileType.Event;
							break;
						case "spritetypes":
							this.ImportFileType = ImportFileType.GFX;
							break;
						case "spritetype":
							this.ImportFileType = ImportFileType.GFX;
							break;
					}

					if (ImportFileType != ImportFileType.Unknown) { break; }
				}


			}

		}
	}
}

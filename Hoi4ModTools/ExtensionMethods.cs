﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hoi4_Mod_Tools {
	static class ExtensionMethods {

		public static bool TryAddKeyValue<T, V>(this Dictionary<T, V> dict,  KeyValuePair<T, V> keyValuePair) {
			if (dict.ContainsKey(keyValuePair.Key)) { return false; }

			try {
				dict.Add(keyValuePair.Key, keyValuePair.Value);
				return true;
			}
			catch (Exception e) {
				return false;
			}
		}

	}
}

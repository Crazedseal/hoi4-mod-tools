﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Hoi4_Mod_Tools {
    static class StringUtils {

        public static Dictionary<String, int> StringKeyToLiteral = new Dictionary<string, int>();
        public static List<String> StringLiterals = new List<string>();

        /// <summary>
        /// Strips a string of embedded strings within it.
        /// </summary>
        /// <param name="str">The string to strip from.</param>
        /// <returns>The string stripped</returns>
        public static String StripStrings(String str) {
            string regEx = "([\"'])(?:(?=(\\\\?))\\2.)*?\\1"; // To strip strings.

            Regex rgx = new Regex(regEx);

            MatchCollection mc = Regex.Matches(str, regEx);

            foreach (Match m in mc) {
                if (!StringKeyToLiteral.ContainsKey(m.Value)) {
                    int literalIndex = StringLiterals.Count;
                    str = str.Replace(m.Value, "[STR_LITERAL_" + literalIndex + "]");
                    StringLiterals.Add(m.Value);
                    StringKeyToLiteral.Add(m.Value, literalIndex);
                }
                else {
                    int literalIndex = StringKeyToLiteral[m.Value];
                    str = str.Replace(m.Value, "[STR_LITERAL_" + literalIndex + "]");
                }

            }

            return str;
        }

        public static String InsertLiterals(String str) {

            string regEx = "\\[STR_LITERAL_\\d+\\]";

            string regEx2 = "(?<=STR_LITERAL_)\\d+";


            MatchCollection mc = Regex.Matches(str, regEx);

            foreach (Match m in mc) {
                // Grab the didget
                int num = Convert.ToInt32(Regex.Match(str, regEx2));
                if (StringLiterals[num] != null) {
                    str = str.Replace(m.Value, StringLiterals[num]);
                }
            }

            return str; 

        }
    }


}

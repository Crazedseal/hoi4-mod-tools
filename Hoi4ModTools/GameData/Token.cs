﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hoi4_Mod_Tools {
    /// <summary>
    /// The current state of building a token
    /// </summary>
    enum TokenState {
        Unknown,
        KeyBuilding,
        StringBuilding,
        ValueBuilding,
        StateFinding,
        CommentBuilding,
        AfterOperator
    }

    enum TokenType {
        KeyValue,
        KeyCollection,
        SingleValue,
        Comment,
        Unknown
    }
    
    class Token {
        public String Key;
		public Boolean IsRoot = false;
        public String Value;
        public Char Operator;
        public String FileFrom;
        public int Depth = 0;
		public int BraceGen = 0;
        public Boolean HasOperator = false;
        public Token Parent;
        public LinkedList<Token> Children = new LinkedList<Token>();
        public TokenType tokenType;
        public Boolean CanBeSingleValue = true;

		
	}

}

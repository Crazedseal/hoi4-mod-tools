﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hoi4_Mod_Tools {
    static class Tokenizer {
        public static Token TokenizeFile(String file) {

            StreamReader fRead;
            fRead = File.OpenText(file);

            String fileText = fRead.ReadToEnd();
            fRead.Close();
            fRead.Dispose();
            String rLine;
            TokenState tokenState = TokenState.Unknown;
            String currentKey = "";
            String currentValue = "";


            HashSet<int> EscapedCharacters = new HashSet<int>();
            HashSet<Char> OperatorChars = new HashSet<char>();
            OperatorChars.Add('=');
            OperatorChars.Add('<');
            OperatorChars.Add('>');
            Token currentScopeToken = new Token();
            Token rootToken = currentScopeToken;
			rootToken.tokenType = TokenType.KeyCollection;
			rootToken.IsRoot = true;
            rootToken.FileFrom = file;
            currentScopeToken.CanBeSingleValue = false;
            currentScopeToken.Key = "Root";
            Token previousToken;
            Token currentToken = new Token();
            currentToken.FileFrom = file;
            currentToken.Parent = currentScopeToken;
            currentScopeToken.Children.AddLast(currentToken);
            int braceCount = 0;

            for (int i = 0; i != fileText.Length; i++) {
                Char currentChar = fileText[i];
                if (currentChar == '{') { braceCount++; }
                else if (currentChar == '}') { braceCount++; }
                switch (tokenState) {
                    case TokenState.Unknown:
                        if (!Char.IsWhiteSpace(currentChar) && currentChar != '#' && currentChar != '}'
                            && currentChar != '\n' && currentChar != '\r') {
                            tokenState = TokenState.KeyBuilding;
                            currentToken.Key += currentChar;
                        }
                        else if (currentChar == '#') {
                            tokenState = TokenState.CommentBuilding;
                            currentToken.tokenType = TokenType.Comment;
                        }
                        else if (currentChar == '}') {
                            currentScopeToken.Children.Remove(currentToken);
                            currentScopeToken = currentScopeToken.Parent;
                            currentToken.Parent = currentScopeToken;
                            currentScopeToken.Children.AddLast(currentToken);
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                        }
                        break;
                    case TokenState.KeyBuilding:
                        if (!Char.IsWhiteSpace(currentChar) && !OperatorChars.Contains(currentChar) && currentChar != '#'
                            && currentChar != '}') {
                            currentToken.Key += currentChar;
                        }
                        else if (currentChar == '#') {

                            currentScopeToken.Children.RemoveLast(); // Destroy it
                            previousToken = currentToken;
                            currentToken = new Token();
                            currentToken.FileFrom = file;
                            currentToken.tokenType = TokenType.Comment;
                            currentToken.Parent = currentScopeToken;
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                            if (currentScopeToken != null) {
                                currentScopeToken.Children.AddLast(currentToken);
                            }
                            tokenState = TokenState.CommentBuilding;
                        }
                        else if (currentChar == '=') {
                            currentToken.CanBeSingleValue = false;
                        }
                        else if (currentChar == '}') {
                            currentToken.tokenType = TokenType.SingleValue;
                            currentToken = new Token();
                            currentToken.FileFrom = file;
                            tokenState = TokenState.Unknown;
                            currentScopeToken = currentScopeToken.Parent;
                            currentToken.Parent = currentScopeToken;
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                        }
                        else {
                            tokenState = TokenState.StateFinding;
                        }
                        break;
                    case TokenState.StringBuilding:
                        if (currentChar == '\\' && !EscapedCharacters.Contains(i)) {
                            if ((i + 1) != fileText.Length) { EscapedCharacters.Add(i + 1); }
                            currentToken.Value += currentChar;
                        }
                        else if (currentChar == '\\' && EscapedCharacters.Contains(i)) {
                            currentToken.Value += currentChar;
                        }
                        else if (currentChar == '"' && !EscapedCharacters.Contains(i)) {
                            currentToken.Value += currentChar;
                            tokenState = TokenState.Unknown;
                            previousToken = currentToken;
                            currentToken = new Token();
                            currentToken.FileFrom = file;
                            currentToken.Parent = currentScopeToken;
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                            if (currentScopeToken != null) {
                                currentScopeToken.Children.AddLast(currentToken);
                            }

                        }
                        else {
                            currentToken.Value += currentChar;
                        }

                        break;
                    case TokenState.ValueBuilding:
                        if (!Char.IsWhiteSpace(currentChar) && currentChar != '#'
                            && currentChar != '}') {
                            currentToken.Value += currentChar;
                        }
                        else if (currentChar == '#') {
                            tokenState = TokenState.CommentBuilding;
                            previousToken = currentToken;
                            currentToken = new Token();
                            currentToken.FileFrom = file;
                            currentToken.tokenType = TokenType.Comment;
                            currentToken.Parent = currentScopeToken;
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                            if (currentScopeToken != null) {
                                currentScopeToken.Children.AddLast(currentToken);
                            }
                        }
                        else if (Char.IsWhiteSpace(currentChar)) {
                            tokenState = TokenState.Unknown;
                            previousToken = currentToken;
                            currentToken = new Token();
                            currentToken.FileFrom = file;
                            currentToken.Parent = currentScopeToken;
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                            if (currentScopeToken != null) {
                                currentScopeToken.Children.AddLast(currentToken);
                            }
                        }
                        else if (currentChar == '}') {
                            currentToken = new Token();
                            currentToken.FileFrom = file;
                            tokenState = TokenState.Unknown;
                            currentScopeToken = currentScopeToken.Parent;
                            currentToken.Parent = currentScopeToken;
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                        }
                        break;
                    case TokenState.StateFinding:
                        if (OperatorChars.Contains(currentChar)) {

                            tokenState = TokenState.AfterOperator;
                            currentToken.HasOperator = true;
                            currentToken.CanBeSingleValue = false;
                            currentToken.Operator = currentChar;
                        }
                        else if (!Char.IsWhiteSpace(currentChar) && !OperatorChars.Contains(currentChar) && currentChar != '#'
                            && currentChar != '}') {
                            currentToken.tokenType = TokenType.SingleValue;
                            currentToken = new Token();
                            currentToken.FileFrom = file;
                            currentToken.Key += currentChar;
                            currentToken.Parent = currentScopeToken;
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                            currentScopeToken.Children.AddLast(currentToken);
                            tokenState = TokenState.Unknown;

                        }
                        else if (currentChar == '#') {
                            currentScopeToken.Children.RemoveLast();
                            currentToken = new Token();
                            currentToken.FileFrom = file;
                            currentToken.Parent = currentScopeToken;
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                            tokenState = TokenState.CommentBuilding;
                            currentToken.tokenType = TokenType.Comment;
                        }
                        else if (currentChar == '}') {
                            currentToken.tokenType = TokenType.SingleValue;
                            currentToken = new Token();
                            currentToken.FileFrom = file;
                            tokenState = TokenState.Unknown;
                            currentScopeToken = currentScopeToken.Parent;
                            currentToken.Parent = currentScopeToken;
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                        }
                        break;
                    case TokenState.AfterOperator:
                        if (!Char.IsWhiteSpace(currentChar) && !OperatorChars.Contains(currentChar) && currentChar != '#'
                            && currentChar != '{' && currentChar != '"') {
                            tokenState = TokenState.ValueBuilding;
                            currentToken.Value += currentChar;
                            currentToken.tokenType = TokenType.KeyValue;
                        }
                        else if (currentChar == '{') {
                            currentScopeToken = currentToken;
                            currentToken.tokenType = TokenType.KeyCollection;
                            tokenState = TokenState.Unknown;
                            currentToken = new Token();
                            currentToken.FileFrom = file;
                            currentToken.Parent = currentScopeToken;
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                            currentScopeToken.Children.AddLast(currentToken);
                        }
                        else if (currentChar == '"') {
                            tokenState = TokenState.StringBuilding;
                            currentToken.tokenType = TokenType.KeyValue;
                            currentToken.Value += currentChar;
                        }


                        break;
                    case TokenState.CommentBuilding:
                        if (currentChar == '\r' && Environment.NewLine == "\r\n" && i + 1 != fileText.Length
                            && fileText[i + 1] == '\n') {
                            previousToken = currentToken;
                            currentToken = new Token();
                            currentToken.FileFrom = file;
                            currentToken.Parent = currentScopeToken;
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                            if (currentScopeToken != null) {
                                currentScopeToken.Children.AddLast(currentToken);
                            }
                            tokenState = TokenState.Unknown; // We in limbowo
                            i++;
                        }
                        else if (currentChar == '\n' && Environment.NewLine == "\n") {
                            currentScopeToken = currentToken;
                            previousToken = currentToken;
                            currentToken = new Token();
                            currentToken.FileFrom = file;
                            currentToken.Parent = currentScopeToken;
                            currentToken.Depth = currentToken.Parent.Depth + 1;
                            if (currentScopeToken != null) {
                                currentScopeToken.Children.AddLast(currentToken);
                            }
                            tokenState = TokenState.Unknown; // We in limbowo
                        }
                        else {
                            currentToken.Value += currentChar;
                        }
                        break;
                }
            }

            return rootToken;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hoi4_Mod_Tools {
    class Mod {
        public String Path { get; set; }
        public String Name { get; set; }
        public String Picture { get; set; }
        public String SupportedVersion { get; set; }
        public String RemoteFileID { get; set; }
        public String Version { get; set; }

        public Boolean ParseSuccess { get; set; }
        public Boolean LoadSuccess { get; set; }

        public List<String> ReplacePaths { get; private set; }
        public List<String> Tags { get; private set; }
        public List<String> Dependancies { get; private set; }

        enum MScope { Tags, Dependancies, None }
        MScope Scope;

        public static Mod ParseModFile(String file) {
            StreamReader fRead;
            try {
                fRead = File.OpenText(file);
            }
            catch (Exception e) {
                return null;
            }
            finally {

            }
            
            Mod toBuild = new Mod();
            toBuild.ParseSuccess = false;
            toBuild.ReplacePaths = new List<string>();
            toBuild.Tags = new List<string>();
            toBuild.Dependancies = new List<string>();
            String currentLine;
            MScope currentScope = MScope.None;
            String currentKey = "";
            String currentValue = "";
            Boolean buildingKey = true;
            Boolean buildingString = false;
            Boolean[] EscapedCharacters;

            try {
                while ((currentLine = fRead.ReadLine()) != null) {
                    if (buildingKey) { currentKey = ""; }
                    if (buildingString) {
                        currentValue = "";
                        buildingString = false;
                        buildingKey = true;
                        currentKey = "";
                    }
                    EscapedCharacters = new bool[currentLine.Length];

                    for (int currentCharPosition = 0;
                        currentCharPosition != currentLine.Length;
                        currentCharPosition++) {
                        if (buildingKey) {
                            if (currentLine[currentCharPosition] == '=') {
                                // Stop building.
                                buildingKey = false;
                                currentKey = currentKey.Trim();
                                continue;
                            }
                            else {
                                currentKey += currentLine[currentCharPosition];
                                continue;
                            }
                        }

                        if (buildingString) {
                            if (currentLine[currentCharPosition] == '\\') {
                                if (!EscapedCharacters[currentCharPosition]) {
                                    if (currentCharPosition != currentLine.Length - 1) {
                                        EscapedCharacters[currentCharPosition + 1] = true;
                                    }
                                    currentValue += currentLine[currentCharPosition];

                                }
                            }
                            else if (currentLine[currentCharPosition] == '"' && !EscapedCharacters[currentCharPosition]) {
                                buildingString = false;
                                if (currentScope == MScope.None) {
                                    switch (currentKey.ToLower()) {
                                        case "path":
                                            toBuild.Path = currentValue;
                                            currentKey = "";
                                            currentValue = "";
                                            buildingKey = true;
                                            buildingString = true;
                                            break;
                                        case "replace_path":
                                            toBuild.ReplacePaths.Add(currentValue);
                                            currentKey = "";
                                            currentValue = "";
                                            buildingKey = true;
                                            buildingString = false;
                                            break;
                                        case "name":
                                            toBuild.Name = currentValue;
                                            buildingKey = true;
                                            buildingString = false;
                                            currentKey = "";
                                            currentValue = "";
                                            break;
                                        case "picture":
                                            toBuild.Picture = currentValue;
                                            buildingKey = true;
                                            buildingString = false;
                                            currentKey = "";
                                            currentValue = "";
                                            break;
                                        case "version":
                                            toBuild.Version = currentValue;
                                            buildingKey = true;
                                            buildingString = false;
                                            currentKey = "";
                                            currentValue = "";
                                            break;
                                        case "supported_version":
                                            toBuild.SupportedVersion = currentValue;
                                            buildingKey = true;
                                            buildingString = false;
                                            currentKey = "";
                                            currentValue = "";
                                            break;
                                        case "remote_file_id":
                                            toBuild.RemoteFileID = currentValue;
                                            buildingKey = true;
                                            buildingString = false;
                                            currentKey = "";
                                            currentValue = "";
                                            break;
                                        default:
                                            buildingKey = true;
                                            buildingString = false;
                                            currentKey = "";
                                            currentValue = "";
                                            break;
                                    }

                                }
                                else if (currentScope == MScope.Dependancies) {
                                    toBuild.Dependancies.Add(currentValue);
                                    currentValue = "";
                                    buildingKey = false;
                                    buildingString = false;
                                }
                                else if (currentScope == MScope.Tags) {
                                    toBuild.Tags.Add(currentValue);
                                    currentValue = "";
                                    buildingKey = false;
                                    buildingString = false;
                                }
                                break;
                            }
                            else {
                                currentValue += currentLine[currentCharPosition];
                                continue;
                            }
                        }

                        if (currentLine[currentCharPosition] == '{') {
                            if (currentKey.ToLower() == "dependencies") {
                                currentScope = MScope.Dependancies;
                            }
                            else if (currentKey.ToLower() == "tags") {
                                currentScope = MScope.Tags;
                            }
                            else {
                                currentScope = MScope.None;
                            }
                        }
                        else if (currentLine[currentCharPosition] == '}') {
                            currentScope = MScope.None;
                            currentKey = "";
                            currentValue = "";
                            buildingKey = true;

                        }

                        if (currentLine[currentCharPosition] == '"') {
                            buildingString = true;
                            continue;
                        }


                    }

                }
            }
            catch (Exception e) {
                toBuild.ParseSuccess = false;
                fRead.Close();
                fRead.Dispose();
                return toBuild;
            }


            fRead.Close();
            fRead.Dispose();
            toBuild.ParseSuccess = true;
            return toBuild;

        }

        public Boolean HasRequiredFields() {
            return
                this.Name != "" && this.Name != null &&
                this.Path != "" && this.Path != null &&
                this.SupportedVersion != "" && this.SupportedVersion != null &&
                this.Version != "" && this.Version != null;
        }

        public override string ToString() {
            String toReturn = "";

            toReturn += "Name: " + Name + Environment.NewLine;
            toReturn += "Version: " + Version + Environment.NewLine;
            toReturn += "Path: " + Path + Environment.NewLine;
            toReturn += "Supported Version: " + SupportedVersion + Environment.NewLine;
            toReturn += "Picture: " + Picture + Environment.NewLine;
            toReturn += "RemoteFileID: " + RemoteFileID + Environment.NewLine;
            toReturn += "Tags: ";
            foreach(String tag in Tags) {
                toReturn += tag + " ";
            }
            toReturn += Environment.NewLine + "Dependancies: ";
            foreach (String depend in Dependancies) {
                toReturn += depend + " ";
            }
            toReturn += Environment.NewLine + "Replace Paths:";
            foreach(String rpath in ReplacePaths) {
                toReturn += rpath + Environment.NewLine;
            }

            return toReturn;
        }

    }

    
}

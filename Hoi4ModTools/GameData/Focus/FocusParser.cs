﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hoi4_Mod_Tools {
    static class FocusParser {

		public static FocusParseResult ParseRootFocusToken(Token t) {
			FocusParseResult parseResult = new FocusParseResult();
			Dictionary<String, Focus> focusDict = new Dictionary<string, Focus>();

			Stack<Token> tokenStack = new Stack<Token>();

			tokenStack.Push(t);

			/*	Context Guide (To help me)
				if current is shared_focus and parent is root
					shared_focus is a new focus.
				if current is shared_focus and parent is focus_tree
					shared_focus value is attached to that tree.
				if current is focus and parent is focus_tree
					focus is a new focus.
				if current is focus and parent is prerequisite
					focus value is prerequisite
            
            */

			FocusTree currentFocusTree = null;
			Focus currentFocus = null;
			HashSet<String> currentPrerequisiteSet = null;
			HashSet<String> currentExclusiveSet = null;
			while (tokenStack.Count != 0) {

				Token currentToken = tokenStack.Pop();
				if (currentToken.Key == null) { continue; }
				String currentKey = currentToken.Key.ToLower();


				if (currentToken.Parent != null) {
					switch (currentToken.Parent.Key.ToLower()) {
						case "root":
							if (currentKey == "focus_tree") {
								// Begin constructing a focus tree.
								currentFocusTree = new FocusTree();
							}
							else if (currentKey == "shared_focus") {
								currentFocus = new Focus(true);
							}
							break;
						case "focus_tree":
							switch (currentKey) {
								case "focus":
									currentFocus = new Focus(false);
									currentFocus.FocusTree = currentFocusTree;
									currentFocusTree.PotentialFocus.Add(currentFocus);
									break;
								case "shared_focus":
									currentFocusTree.UnresolvedFocus.Add(currentToken.Value);
									break;
								case "id":
									currentFocusTree.ID = currentToken.Value;
									break;
								case "default":
									string dlower = currentToken.Value.ToLower();
									if (dlower == "yes") {
										currentFocus.CancelIfInvalid = true;
									}
									else if (dlower == "no") {
										currentFocus.CancelIfInvalid = false;
									}
									else {
										// shrug
									}
									break;
								case "country":
									currentFocusTree.CountrySelectModifier = currentToken;
									break;
							}
							break;
						case "shared_focus":
							if (currentFocus == null) { currentFocus = new Focus(true); }
							goto case "focus";
						case "focus":
							if (currentFocus == null) { currentFocus = new Focus(false); }
							switch (currentKey) {
								case "id":
									if (focusDict.ContainsKey(currentToken.Value)) { break; }
									//System.Diagnostics.Debug.WriteLine(currentToken.Value);
									currentFocus.ID = currentToken.Value;
									focusDict[currentToken.Value] = currentFocus;
									break;
								case "icon":
									currentFocus.Icon = currentToken.Value;
									break;
								case "relative_position_id":
									currentFocus.RelativePositionTo = currentToken.Value;
									break;
								case "cost":
									try {
										currentFocus.Cost = Convert.ToInt32(currentToken.Value);
									}
									catch (Exception e) {
										currentFocus.PotentialErrors.Add("This focus cost was not an integer. Cost: " + currentToken.Value);
									}
									break;
								case "cancel_if_invalid":
									string lower = currentToken.Value.ToLower();
									if (lower == "yes") {
										currentFocus.CancelIfInvalid = true;
									}
									else if (lower == "no") {
										currentFocus.CancelIfInvalid = false;
									}
									else {
										currentFocus.PotentialErrors.Add("Unexpected value for Cancel_if_invalid. Value: " + currentToken.Value);
									}
									break;
								case "continue_if_invalid":
									string cif_lower = currentToken.Value.ToLower();
									if (cif_lower == "yes") {
										currentFocus.ContinueIfInvalid = true;
									}
									else if (cif_lower == "no") {
										currentFocus.ContinueIfInvalid = false;
									}
									else {
										currentFocus.PotentialErrors.Add("Unexpected value for continue_if_invalid. Value: " + currentToken.Value);
									}
									break;
								case "available_if_capitulated":
									string aic_lower = currentToken.Value.ToLower();
									if (aic_lower == "yes") {
										currentFocus.ContinueIfInvalid = true;
									}
									else if (aic_lower == "no") {
										currentFocus.ContinueIfInvalid = false;
									}
									else {
										currentFocus.PotentialErrors.Add("Unexpected value for available_if_capitulated. Value: " + currentToken.Value);
									}
									break;
								case "x":
									try {
										currentFocus.Location.X = Convert.ToInt32(currentToken.Value);
									}
									catch (Exception e) {
										currentFocus.PotentialErrors.Add("This focus location X was not an integer. Cost: " + currentToken.Value);
									}
									break;
								case "y":
									try {
										currentFocus.Location.Y = Convert.ToInt32(currentToken.Value);
									}
									catch (Exception e) {
										currentFocus.PotentialErrors.Add("This focus location Y was not an integer. Cost: " + currentToken.Value);
									}
									break;
								case "select_effect":
									currentFocus.SelectEffect = currentToken;
									break;
								case "completion_reward":
									currentFocus.CompletionReward = currentToken;
									break;
								case "ai_will_do":
									
									currentFocus.AIWillDo = currentToken;
									break;
								case "available":
									currentFocus.Available = currentToken;
									break;
								case "bypass":
									currentFocus.Bypass = currentToken;
									break;
								case "prerequisite":
									currentPrerequisiteSet = new HashSet<string>();
									currentFocus.PrerequisiteSets.Add(currentPrerequisiteSet);
									break;
								case "mutually_exclusive":
									// ???? am I dumb.... No???
									break;
							}
							break;
						case "prerequisite":
							if (currentKey == "focus") {
								// Just to ensure that the current key is correct.
								// If it's not a correct token then this will ignore it.

								currentPrerequisiteSet.Add(currentToken.Value);
							}
							break;
						case "mutually_exclusive":
							if (currentKey == "focus") {
								currentFocus.MutuallyExclusiveTo.Add(currentToken.Value);

							}
							break;
						case "continuous_focus_position":
							try {
								if (currentToken.Key.ToLower() == "x") {
									currentFocusTree.ContinuousFocus.X = Convert.ToInt32(currentToken.Value);
								}
								else if (currentToken.Key.ToLower() == "y") {
									currentFocusTree.ContinuousFocus.X = Convert.ToInt32(currentToken.Value);
								}
								else {
									// Shrugs
								}
							}
							catch (Exception e) { }
							break;
					}
				}

				foreach (Token token in currentToken.Children) {
					tokenStack.Push(token);
				}


			}

			parseResult.focusDict = focusDict;
			parseResult.Success = true;

			return parseResult;

		}

        public static FocusParseResult ParseFocusFile(String filepath) {
            Token rootToken;
            FocusParseResult parseResult = new FocusParseResult();
            Dictionary<String, Focus> focusDict = new Dictionary<string, Focus>();

            try {
                rootToken = Tokenizer.TokenizeFile(filepath);
            }
            catch (Exception e) {
                parseResult.FailureReason = "Failed to tokenize file: " + filepath + " due to the following: " + Environment.NewLine;
                parseResult.FailureReason += e.Message;
                parseResult.Success = false;
                return parseResult;
            }

			parseResult = ParseRootFocusToken(rootToken);

			return parseResult;
			
        }

        
        

    }
}

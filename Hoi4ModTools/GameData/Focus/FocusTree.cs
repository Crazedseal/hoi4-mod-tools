﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hoi4_Mod_Tools {
    class FocusTree {
        public Dictionary<String, Focus> focuses = new Dictionary<string, Focus>();
        public String ID;
        public HashSet<Focus> SharedFocus = new HashSet<Focus>();
		public HashSet<String> UnresolvedFocus = new HashSet<string>();
        public Token CountrySelectModifier;
		public Point ContinuousFocus = new Point();
		public HashSet<Focus> PotentialFocus = new HashSet<Focus>();



	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Hoi4_Mod_Tools {
    class Focus {
		public static Dictionary<String, Focus> loadedFocus = new Dictionary<string, Focus>();

        public Boolean IsShared { get; private set; }

        public String ID { get; set; }
        public String Tree { get; set; }
		public FocusTree FocusTree { get; set; }
        public String Icon { get; set; }
        public int Cost { get; set; }
        public Token AIWillDo { get; set; }
		public Boolean IsResolved { private set; get; }

        public String RelativePositionTo { get; set; }
		public Focus RelativePositionToFocus { get; set; }
        public Point Location { get; set; }
        public Point RelativeLocation() {
			if (RelativePositionToFocus == null) { return Location; }
			else { return RelativePositionToFocus.Location + this.Location; }
		}
        public Token Available { get; set; }
        public Token Bypass { get; set; }
        public Boolean CancelIfInvalid { get; set; }
        public Boolean ContinueIfInvalid { get; set; }
        public Boolean AvailableIfCapitulated { get; set; }
        public Boolean IsDynamic { get; set; }
        public Token CompletionReward { get; set; }
        public Token SelectEffect { get; set; }
        public HashSet<String> MutuallyExclusiveTo { get; set; }
		public HashSet<Focus> MutuallyExclusiveToFocus { get; set; }

        public List<String> WillLeadToWarWith { get; set; }

        public HashSet<String> IsPrerequisiteTo { get; set; }
		public HashSet<Focus> IsPrerequisiteToFocus { get; set; }

		public List<HashSet<String>> PrerequisiteSets { get; set; }
		public List<HashSet<Focus>> PrerequisiteFocusSets { get; set; }
		public List<String> PotentialErrors { get; set; }

		public Focus(Boolean shared) {
			this.IsShared = shared;
			this.Location = new Point(0, 0);
			this.PrerequisiteSets = new List<HashSet<string>>();
			this.WillLeadToWarWith = new List<string>();
			this.MutuallyExclusiveTo = new HashSet<string>();
			this.IsPrerequisiteTo = new HashSet<string>();
			this.IsPrerequisiteToFocus = new HashSet<Focus>();
			this.Cost = 10;
			this.CancelIfInvalid = true;
			this.ContinueIfInvalid = true;
			this.AvailableIfCapitulated = true;
			this.IsDynamic = false;
			this.PotentialErrors = new List<string>();
		}

		private string BoolToYesNo(Boolean bb) {
			if (bb) { return "yes"; }
			else { return "no"; }
		}

		public override string ToString() {
			String toBuild = "";
			String xTab = "\t";

			toBuild += this.IsShared ? "shared_focus = {" : "focus = {";
			toBuild += Environment.NewLine;

			toBuild += $"\tid = {this.ID}{Environment.NewLine}";
			toBuild += this.Icon != null ? $"\ticon = {this.Icon}{Environment.NewLine}" : "";
			toBuild += this.Location != null ? $"\tx = {this.Location.X}{Environment.NewLine}\ty = {this.Location.Y}{Environment.NewLine}" : "";
			toBuild += $"\tcost = {this.Cost}{Environment.NewLine}";

			if (this.Available != null) {
				toBuild += TokenUtil.TokenToString(this.Available, true, xTab) + Environment.NewLine;
			}

			toBuild += $"\tavailable_if_capitulated = {BoolToYesNo(this.AvailableIfCapitulated)}{Environment.NewLine}";
			toBuild += $"\tcontinue_if_invalid = {BoolToYesNo(this.ContinueIfInvalid)}{Environment.NewLine}";
			toBuild += $"\tcancel_if_invalid = {BoolToYesNo(this.CancelIfInvalid)}{Environment.NewLine}";

			toBuild += this.RelativePositionTo != "" && this.RelativePositionTo != null ? $"\trelative_position_id = {this.RelativePositionTo}{Environment.NewLine}" : "";


			
			if (this.PrerequisiteSets.Count > 0) {
				foreach (var set in this.PrerequisiteSets) {
					toBuild += "\tprerequisite = { " + Environment.NewLine;
					foreach (var prereq in set) {
						toBuild += $"\t\tfocus = {prereq}{Environment.NewLine}";
					}
					toBuild += "\t}" + Environment.NewLine; ;
				}
			}
			

			if (this.MutuallyExclusiveTo.Count > 0) {
				toBuild += "\tmutually_exclusive = { ";
				foreach (var excl in this.MutuallyExclusiveTo) {
					toBuild += $"focus = {excl} ";
				}
				toBuild += "}" + Environment.NewLine;
			}

			if (this.AIWillDo != null) {

				toBuild += TokenUtil.TokenToString(this.AIWillDo, true, xTab) + Environment.NewLine;

			}

			if (this.SelectEffect != null) {
				toBuild += TokenUtil.TokenToString(this.SelectEffect, true, xTab) + Environment.NewLine;
			}

			if (this.CompletionReward != null) {
				toBuild += TokenUtil.TokenToString(this.CompletionReward, true, xTab) + Environment.NewLine;
			}

			if (this.Bypass != null) {
				toBuild += TokenUtil.TokenToString(this.Bypass, true, xTab) + Environment.NewLine;
			}

			if (this.WillLeadToWarWith.Count > 0) {
				foreach (var to in this.WillLeadToWarWith) {
					toBuild += $"\twill_lead_to_war_with = {to}{Environment.NewLine}";
				}
			}

			toBuild += "}";


			return toBuild;
		}

		public Boolean HasRelativePosition() {
			if (IsResolved) {
				if (RelativePositionToFocus != null) { return true; }
				else { return false; }
			}
			else {
				if (RelativePositionTo != null) {
					return true;
				}
				else { return false; }
			}
		}

		public void BindToRelativeFocus(Focus relativeBase, Boolean resolveNewLocation = false) {
			if (resolveNewLocation) {
				if (this.RelativePositionToFocus != null) {
					this.Location = this.RelativeLocation();
				}

				this.Location.X = this.Location.X - relativeBase.Location.X;
				this.Location.Y = this.Location.Y - relativeBase.Location.Y;
			}
			this.RelativePositionTo = relativeBase.ID;
			this.RelativePositionToFocus = relativeBase;
			

		}

		public static void ResolveConnections( ) {
			foreach (KeyValuePair<String, Focus> resolvePair in loadedFocus) {
				Focus toResolve = resolvePair.Value;
				toResolve.PrerequisiteFocusSets = new List<HashSet<Focus>>();
				toResolve.MutuallyExclusiveToFocus = new HashSet<Focus>();
				//System.Diagnostics.Debug.WriteLine("Resolving: " + toResolve.ID);
				if (toResolve.RelativePositionTo != null) {
					if (loadedFocus.ContainsKey(toResolve.RelativePositionTo)) {
						toResolve.RelativePositionToFocus = loadedFocus[toResolve.RelativePositionTo];
						//System.Diagnostics.Debug.WriteLine("Successfully resolved relative position to for focus " + toResolve.ID + " towards " + toResolve.RelativePositionTo);
					}
					else {
						toResolve.PotentialErrors.Add(
							String.Format("Focus ID: {0} cannot resolve relative focus id of {1} to a loaded focus.", toResolve.ID, toResolve.RelativePositionTo
							));
						System.Diagnostics.Debug.WriteLine(String.Format("Focus ID: {0} cannot resolve relative focus id of {1} to a loaded focus.", toResolve.ID, toResolve.RelativePositionTo
							));
					}
				}

				foreach (HashSet<String> prerequisiteSetsToResolve in toResolve.PrerequisiteSets) {
					HashSet<Focus> resolvedSet = new HashSet<Focus>();
					toResolve.PrerequisiteFocusSets.Add(resolvedSet);
					foreach (String stringToResolve in prerequisiteSetsToResolve) {
						if (loadedFocus.ContainsKey(stringToResolve)) {
							resolvedSet.Add(loadedFocus[stringToResolve]);
							loadedFocus[stringToResolve].IsPrerequisiteToFocus.Add(toResolve);
							//System.Diagnostics.Debug.WriteLine($"Resolved {toResolve.ID} prerequisite {stringToResolve}");
						}
						else {
							toResolve.PotentialErrors.Add(
						String.Format("Focus ID: {0} cannot resolve prerequisite focus id of {1} to a loaded focus.", toResolve.ID, stringToResolve
						));
						}
					}
				}

				foreach (String resolveString in toResolve.MutuallyExclusiveTo) {
					if (loadedFocus.ContainsKey(resolveString)) {
						toResolve.MutuallyExclusiveToFocus.Add(loadedFocus[resolveString]);
					}
					else {
						toResolve.PotentialErrors.Add(
						String.Format("Focus ID: {0} cannot resolve mutually exclusive focus id of {1} to a loaded focus.", toResolve.ID, resolveString
						));
					}
				}

				toResolve.IsResolved = true;
			}
			

		}
    }

    class Point {
		public int X;
		public int Y;

        public Point(int X = 0, int Y = 0) {
            this.X = X;
            this.Y = Y;
        }

		public static Point operator + (Point p1, Point p2) {
		return new Point(p1.X + p2.X, p1.Y + p2.Y);

		}
    }

    class FocusParseResult {
        public Boolean Success = false;
        public Dictionary<String, Focus> focusDict = new Dictionary<string, Focus>();
        public String FailureReason;
		
    }
}

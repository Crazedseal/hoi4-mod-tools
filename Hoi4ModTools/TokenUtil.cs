﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hoi4_Mod_Tools {
	class TokenUtil {
		public static string TokenToString(Token token, Boolean reset_tabs = true, String extraTab = "") {
			String LBuild = "";
			Dictionary<Token, int> tabDepth = new Dictionary<Token, int>();
			Dictionary<Token, int> braceGen = new Dictionary<Token, int>();
			tabDepth[token] = 0;
			braceGen[token] = 0;
			
			Stack<Token> tStack = new Stack<Token>();
			tStack.Push(token);
			while (tStack.Count != 0) {
				Token cToken = tStack.Pop();
				LBuild += extraTab;
				if (cToken.Children.Count > 0 && cToken.tokenType == TokenType.KeyCollection) {
					//Console.WriteLine(cToken.Key + cToken.Children.Last.Value.Key);
					if (!braceGen.ContainsKey(cToken)) { braceGen[cToken] = 0; }
					braceGen[cToken.Children.Last.Value] = braceGen[cToken] + 1;
					braceGen[cToken] = 0;
				}
				if (reset_tabs) {
					for (int tab = tabDepth[cToken]; tab > 0; tab--) {
						LBuild += "\t";
					}
				}
				else {
					for (int tab = cToken.Depth - 1; tab > 0; tab--) {
						LBuild += "\t";
					}
				}
				switch (cToken.tokenType) {
					case TokenType.Comment:
						LBuild += "# " + cToken.Value + Environment.NewLine;
						break;
					case TokenType.KeyCollection:
						LBuild += cToken.Key + " = {";
						if (cToken.Children.Count == 0) { LBuild += " }" + Environment.NewLine; }
						else { LBuild += Environment.NewLine; }
						break;
					case TokenType.KeyValue:
						LBuild += cToken.Key + " = " + cToken.Value + Environment.NewLine;
						break;
					case TokenType.SingleValue:
						LBuild += cToken.Key + " ";
						break;
					case TokenType.Unknown:

						break;
					default:
						LBuild += cToken.Key + Environment.NewLine;
						break;

				}
				foreach (Token child in cToken.Children.Reverse()) {
					tabDepth[child] = tabDepth[cToken] + 1;
					tStack.Push(child);
				}
				if (cToken.Parent == null) { continue; }


				if (braceGen.ContainsKey(cToken)) {
					for (int i = 0; i != braceGen[cToken]; i++) {
						for (int j = 1; j < tabDepth[cToken] - i; j++) {
							LBuild += "\t";
						}
						LBuild += extraTab + "}" + Environment.NewLine;
					}
				}

				braceGen[cToken] = 0;

			}
			if (token.IsRoot) {
				LBuild = LBuild.Remove(0, LBuild.IndexOf(Environment.NewLine) + (Environment.NewLine.Length));
				LBuild = LBuild.Substring(0, LBuild.Length - (1 + Environment.NewLine.Length));
			}

			return LBuild;


		}
	}
}

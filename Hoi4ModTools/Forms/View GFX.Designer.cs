﻿namespace Hoi4_Mod_Tools {
    partial class gfxViewWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
			this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.autoSearchChkBox = new System.Windows.Forms.CheckBox();
			this.clearButton = new System.Windows.Forms.Button();
			this.searchButton = new System.Windows.Forms.Button();
			this.viewGFXPanel1 = new Hoi4_Mod_Tools.ViewGFXPanel();
			this.importGFXButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// hScrollBar1
			// 
			this.hScrollBar1.Location = new System.Drawing.Point(12, 540);
			this.hScrollBar1.Name = "hScrollBar1";
			this.hScrollBar1.Size = new System.Drawing.Size(635, 17);
			this.hScrollBar1.TabIndex = 1;
			// 
			// vScrollBar1
			// 
			this.vScrollBar1.LargeChange = 100;
			this.vScrollBar1.Location = new System.Drawing.Point(650, 12);
			this.vScrollBar1.Name = "vScrollBar1";
			this.vScrollBar1.Size = new System.Drawing.Size(17, 521);
			this.vScrollBar1.SmallChange = 33;
			this.vScrollBar1.TabIndex = 2;
			// 
			// listBox1
			// 
			this.listBox1.DisplayMember = "Name";
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Location = new System.Drawing.Point(670, 61);
			this.listBox1.Name = "listBox1";
			this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.listBox1.Size = new System.Drawing.Size(287, 459);
			this.listBox1.TabIndex = 3;
			this.listBox1.ValueMember = "Name";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(670, 35);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(287, 20);
			this.textBox1.TabIndex = 4;
			this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// autoSearchChkBox
			// 
			this.autoSearchChkBox.AutoSize = true;
			this.autoSearchChkBox.Location = new System.Drawing.Point(848, 12);
			this.autoSearchChkBox.Name = "autoSearchChkBox";
			this.autoSearchChkBox.Size = new System.Drawing.Size(80, 17);
			this.autoSearchChkBox.TabIndex = 5;
			this.autoSearchChkBox.Text = "Autosearch";
			this.autoSearchChkBox.UseVisualStyleBackColor = true;
			// 
			// clearButton
			// 
			this.clearButton.Location = new System.Drawing.Point(759, 8);
			this.clearButton.Name = "clearButton";
			this.clearButton.Size = new System.Drawing.Size(83, 23);
			this.clearButton.TabIndex = 6;
			this.clearButton.Text = "Clear Search";
			this.clearButton.UseVisualStyleBackColor = true;
			this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
			// 
			// searchButton
			// 
			this.searchButton.Location = new System.Drawing.Point(670, 8);
			this.searchButton.Name = "searchButton";
			this.searchButton.Size = new System.Drawing.Size(83, 23);
			this.searchButton.TabIndex = 7;
			this.searchButton.Text = "Search";
			this.searchButton.UseVisualStyleBackColor = true;
			this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
			// 
			// viewGFXPanel1
			// 
			this.viewGFXPanel1.DrawInterval = 17;
			this.viewGFXPanel1.Location = new System.Drawing.Point(12, 12);
			this.viewGFXPanel1.MouseHoverUpdatesOnly = false;
			this.viewGFXPanel1.Name = "viewGFXPanel1";
			this.viewGFXPanel1.Size = new System.Drawing.Size(635, 521);
			this.viewGFXPanel1.TabIndex = 0;
			this.viewGFXPanel1.Text = "viewGFXPanel1";
			this.viewGFXPanel1.Click += new System.EventHandler(this.viewGFXPanel1_Click);
			this.viewGFXPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.viewGFXPanel1_Paint);
			// 
			// importGFXButton
			// 
			this.importGFXButton.Location = new System.Drawing.Point(670, 526);
			this.importGFXButton.Name = "importGFXButton";
			this.importGFXButton.Size = new System.Drawing.Size(83, 23);
			this.importGFXButton.TabIndex = 8;
			this.importGFXButton.Text = "Import GFX";
			this.importGFXButton.UseVisualStyleBackColor = true;
			this.importGFXButton.Click += new System.EventHandler(this.importGFXButton_Click);
			// 
			// gfxViewWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(974, 564);
			this.Controls.Add(this.importGFXButton);
			this.Controls.Add(this.searchButton);
			this.Controls.Add(this.clearButton);
			this.Controls.Add(this.autoSearchChkBox);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.vScrollBar1);
			this.Controls.Add(this.hScrollBar1);
			this.Controls.Add(this.viewGFXPanel1);
			this.Name = "gfxViewWindow";
			this.Text = "Hearts of Iron IV Mod Tools - View GFX";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.gfxViewWindow_FormClosing);
			this.Load += new System.EventHandler(this.gfxViewWindow_Load);
			this.VisibleChanged += new System.EventHandler(this.gfxViewWindow_VisibleChanged);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.VScrollBar vScrollBar1;
        internal ViewGFXPanel viewGFXPanel1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.CheckBox autoSearchChkBox;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button searchButton;
        protected internal System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Button importGFXButton;
	}
}
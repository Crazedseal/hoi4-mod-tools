﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hoi4_Mod_Tools {
    public partial class gfxViewWindow : Form {
        public gfxViewWindow() {
            InitializeComponent();
			currentImportGFXWindow = new Forms.Import_GFX(this);

		}

        HashSet<GFXSprite> sprites = new HashSet<GFXSprite>();
        List<GFXSprite> allSprites = new List<GFXSprite>();
        HashSet<GFXSprite> boxSprites = new HashSet<GFXSprite>();
		Forms.Import_GFX currentImportGFXWindow;

		private void viewGFXPanel1_Click(object sender, EventArgs e) {

        }

        private void gfxViewWindow_FormClosing(object sender, FormClosingEventArgs e) {

            if (e.CloseReason == CloseReason.UserClosing) {
                e.Cancel = true;
                this.Hide();
                this.Enabled = false;
            }
            else {
            }
        }

        private void gfxViewWindow_Load(object sender, EventArgs e) {
            GFXSprite.OnSpriteAdd += GFXSprite_OnSpriteAdd;
        }

        private void GFXSprite_OnSpriteAdd(GFXSprite sprite) {
            if (!this.sprites.Contains(sprite)) {
                this.allSprites.Add(sprite);
                this.sprites.Add(sprite);

            }
        }

        private void viewGFXPanel1_Paint(object sender, PaintEventArgs e) {
            
        }

        internal void updateScrollbar() {
            this.hScrollBar1.Maximum = this.viewGFXPanel1.maxScrollBarX;
            this.vScrollBar1.Maximum = this.viewGFXPanel1.maxScrollBarY;

            this.viewGFXPanel1.scrollBarX = this.hScrollBar1.Value;
            this.viewGFXPanel1.scrollBarY = this.vScrollBar1.Value;
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {

            if (autoSearchChkBox.Checked) {
                searchBox();
            }

        }

        private void clearButton_Click(object sender, EventArgs e) {
            this.textBox1.Text = String.Empty;
        }

        private void searchButton_Click(object sender, EventArgs e) {
            searchBox();
        }

		internal void addItem(GFXSprite sprite) {
			boxSprites.Add(sprite);
			listBox1.Items.Add(sprite);

		}

        private void searchBox() {
            List<object> newItemBox = new List<object>();
            HashSet<GFXSprite> toRemove = new HashSet<GFXSprite>();

            foreach (var sprite in listBox1.Items) {
                if (sprite.GetType() == typeof(GFXSprite)) {
                    GFXSprite gfxSprite = (GFXSprite)sprite;
                    if (!gfxSprite.Name.Contains(this.textBox1.Text)) {
                            boxSprites.Remove(gfxSprite);
                            toRemove.Add(gfxSprite);
                    }
                }
            }

            int i = 0;
            while (i < listBox1.Items.Count) {
                if (toRemove.Contains(listBox1.Items[i])) { listBox1.Items.RemoveAt(i); }
                else { i++;  }
            }

            foreach (var sprites in allSprites) {
                if (!boxSprites.Contains(sprites) && !toRemove.Contains(sprites) && sprites.Name.Contains(this.textBox1.Text)) {
                    listBox1.Items.Add(sprites);
                    boxSprites.Add(sprites);
                }
            }
        }

        private void gfxViewWindow_VisibleChanged(object sender, EventArgs e) {
            this.listBox1.Items.Clear();
            foreach (var sprite in allSprites) {
                listBox1.Items.Add(sprite);
                boxSprites.Add(sprite);
            }
        }

		private void importGFXButton_Click(object sender, EventArgs e) {
			if (!currentImportGFXWindow.Visible) {
				currentImportGFXWindow.Dispose();
				currentImportGFXWindow = new Forms.Import_GFX(this);
				currentImportGFXWindow.ShowDialog();
			}
		}
	}
}

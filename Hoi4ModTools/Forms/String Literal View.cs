﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hoi4_Mod_Tools {
    public partial class String_Literal_View : Form {
        public String_Literal_View() {
            InitializeComponent();
        }

        private void String_Literal_View_Load(object sender, EventArgs e) {
            for (int i = 0; i != StringUtils.StringLiterals.Count; i++) {
                String[] con = new string[2];

                con[0] = "[STR_LITERAL_" + i + "]";
                con[1] = StringUtils.StringLiterals[i];

                ListViewItem listViewItem = new ListViewItem(con);
                this.listView1.Items.Add(listViewItem);
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e) {

        }
    }
}

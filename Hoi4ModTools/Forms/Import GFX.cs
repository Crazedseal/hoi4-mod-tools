﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hoi4_Mod_Tools.Forms {
	public partial class Import_GFX : Form {
		public Import_GFX(gfxViewWindow parent) {
			InitializeComponent();
			this.Parent = parent;
		}

		gfxViewWindow Parent;

		private void Import_GFX_Load(object sender, EventArgs e) {

		}

		private void button2_Click(object sender, EventArgs e) {
			// Import GFX
			textBox1.Text = textBox1.Text.Trim();
			textBox2.Text = textBox2.Text.Trim();
			if (textBox1.Text == "") {
				MessageBox.Show($"No file path detected. Please enter a file path.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			if (!File.Exists(textBox1.Text)) {
				MessageBox.Show($"File {textBox1.Text} does not exist. Please check the file path.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			GFXSprite import = new GFXSprite(textBox2.Text, textBox1.Text);

			import.LoadTexture(Parent.viewGFXPanel1.GraphicsDevice);

			if (import.HasFailedToLoad) {
				MessageBox.Show($"An error has occured. Unable to import the file: {textBox1.Text}. Please ensure that the path and format of the file is correct.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else {
				if (GFXSprite.GFXSprites.ContainsKey(textBox2.Text) && this.overrideCurrentChkBox.Checked) {
					if (GFXSprite.GFXSprites[textBox2.Text].Texture != null) {
						GFXSprite.GFXSprites[textBox2.Text].Texture.Dispose();
					}
					
					GFXSprite.AddGFXSprite(import);
					Parent.addItem(import);
				}
				else if (GFXSprite.GFXSprites.ContainsKey(textBox2.Text) && !this.overrideCurrentChkBox.Checked) {
					MessageBox.Show($"An error has occured. Unable to import the file: {textBox1.Text}. A GFX Sprite with the Name {textBox2.Text} already exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else {
					GFXSprite.AddGFXSprite(import);

					Parent.addItem(import);
				}
			}

			this.Hide();
			
		}

		private void button3_Click(object sender, EventArgs e) {
			this.Close();
			this.Dispose();
		}

		private void button1_Click(object sender, EventArgs e) {

			OpenFileDialog open = new OpenFileDialog();
			open.Filter = "Image Files(*.BMP;*.JPG;*.PNG;*.TGA;*.DDS)|*.BMP;*.JPG;*.PNG;*.TGA;*.DDS|DDS Image|*.DDS|Common Image(*.BMP;*.JPG;.PNG)|*.BMP;.*JPG;*.PNG|All files (*.*)|*.*";
			open.ShowDialog();

			string path = open.FileName;

			if (path == "") {
				return;
			}

			textBox1.Text = path;
			textBox2.Text = "GFX_" + open.SafeFileName.Replace(' ', '_');


		}
	}
}

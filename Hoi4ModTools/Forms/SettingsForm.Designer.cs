﻿namespace Hoi4_Mod_Tools {
    partial class SettingsForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.autoLoadTextures = new System.Windows.Forms.CheckBox();
            this.autoLoadTexturesTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.rememberDirectoryChkBox = new System.Windows.Forms.CheckBox();
            this.rememberDirTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.iconPerRowLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.hideMainChkBox = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // autoLoadTextures
            // 
            this.autoLoadTextures.AutoSize = true;
            this.autoLoadTextures.Location = new System.Drawing.Point(12, 12);
            this.autoLoadTextures.Name = "autoLoadTextures";
            this.autoLoadTextures.Size = new System.Drawing.Size(112, 17);
            this.autoLoadTextures.TabIndex = 0;
            this.autoLoadTextures.Text = "Autoload Textures";
            this.autoLoadTexturesTooltip.SetToolTip(this.autoLoadTextures, "When parsing GFX files automatically load those textures.");
            this.autoLoadTextures.UseVisualStyleBackColor = true;
            this.autoLoadTextures.CheckedChanged += new System.EventHandler(this.autoLoadTextures_CheckedChanged);
            // 
            // autoLoadTexturesTooltip
            // 
            this.autoLoadTexturesTooltip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.autoLoadTexturesTooltip.ToolTipTitle = "Texture";
            // 
            // rememberDirectoryChkBox
            // 
            this.rememberDirectoryChkBox.AutoSize = true;
            this.rememberDirectoryChkBox.Location = new System.Drawing.Point(12, 35);
            this.rememberDirectoryChkBox.Name = "rememberDirectoryChkBox";
            this.rememberDirectoryChkBox.Size = new System.Drawing.Size(130, 17);
            this.rememberDirectoryChkBox.TabIndex = 1;
            this.rememberDirectoryChkBox.Text = "Remember Directories";
            this.rememberDirTooltip.SetToolTip(this.rememberDirectoryChkBox, "Store the last loaded directories and mods and load them on start.");
            this.rememberDirectoryChkBox.UseVisualStyleBackColor = true;
            this.rememberDirectoryChkBox.CheckedChanged += new System.EventHandler(this.rememberDirectoryChkBox_CheckedChanged);
            // 
            // rememberDirTooltip
            // 
            this.rememberDirTooltip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.rememberDirTooltip.ToolTipTitle = "Texture";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(87, 58);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(37, 20);
            this.numericUpDown1.TabIndex = 2;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // iconPerRowLabel
            // 
            this.iconPerRowLabel.AutoSize = true;
            this.iconPerRowLabel.Location = new System.Drawing.Point(9, 60);
            this.iconPerRowLabel.Name = "iconPerRowLabel";
            this.iconPerRowLabel.Size = new System.Drawing.Size(72, 13);
            this.iconPerRowLabel.TabIndex = 4;
            this.iconPerRowLabel.Text = "GFX Per Row";
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button1.Location = new System.Drawing.Point(0, 91);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(346, 21);
            this.button1.TabIndex = 5;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // hideMainChkBox
            // 
            this.hideMainChkBox.AutoSize = true;
            this.hideMainChkBox.Location = new System.Drawing.Point(218, 12);
            this.hideMainChkBox.Name = "hideMainChkBox";
            this.hideMainChkBox.Size = new System.Drawing.Size(116, 17);
            this.hideMainChkBox.TabIndex = 6;
            this.hideMainChkBox.Text = "Hide Main Window";
            this.toolTip1.SetToolTip(this.hideMainChkBox, "When opening another window hide the main menu window.");
            this.hideMainChkBox.UseVisualStyleBackColor = true;
            this.hideMainChkBox.CheckedChanged += new System.EventHandler(this.hideMainChkBox_CheckedChanged);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 112);
            this.ControlBox = false;
            this.Controls.Add(this.hideMainChkBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.iconPerRowLabel);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.rememberDirectoryChkBox);
            this.Controls.Add(this.autoLoadTextures);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.Text = "Hearts of Iron IV Mod Tools - Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.VisibleChanged += new System.EventHandler(this.SettingsForm_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox autoLoadTextures;
        private System.Windows.Forms.ToolTip autoLoadTexturesTooltip;
        private System.Windows.Forms.CheckBox rememberDirectoryChkBox;
        private System.Windows.Forms.ToolTip rememberDirTooltip;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label iconPerRowLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox hideMainChkBox;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
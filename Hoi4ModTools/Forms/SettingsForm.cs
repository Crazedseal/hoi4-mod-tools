﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hoi4_Mod_Tools {
    public partial class SettingsForm : Form {
        public SettingsForm(MenuForm parent) {
            InitializeComponent();
            this.parentForm = parent;
        }

        MenuForm parentForm;

        private void SettingsForm_Load(object sender, EventArgs e) {
            
        }

        private void autoLoadTextures_CheckedChanged(object sender, EventArgs e) {
            if (autoLoadTextures.Checked) {
                Settings.AutoLoadTextures = true;
            } else { Settings.AutoLoadTextures = false; }
        }

        private void rememberDirectoryChkBox_CheckedChanged(object sender, EventArgs e) {
            if (rememberDirectoryChkBox.Checked) {
                Settings.RememberDirectories = true;
            } else { Settings.RememberDirectories = false; }
        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e) {
            if (e.CloseReason == CloseReason.UserClosing) {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            this.Hide();
        }

        private void SettingsForm_VisibleChanged(object sender, EventArgs e) {
            this.rememberDirectoryChkBox.Checked = Settings.RememberDirectories;
            this.autoLoadTextures.Checked = Settings.AutoLoadTextures;
            this.numericUpDown1.Value = 1;

            if (this.Visible) { if (Settings.OpeningWindowsHidesMain) { parentForm.Hide(); } }
            else { if (Settings.OpeningWindowsHidesMain) { parentForm.Show(); } }
        }

        private void hideMainChkBox_CheckedChanged(object sender, EventArgs e) {
            Settings.OpeningWindowsHidesMain = this.hideMainChkBox.Checked;
            if (Settings.OpeningWindowsHidesMain) { parentForm.Hide(); }
            else { parentForm.Show(); }
            
        }
    }
}

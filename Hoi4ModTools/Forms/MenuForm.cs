﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace Hoi4_Mod_Tools {
    public partial class MenuForm : Form {
        public MenuForm() {
            InitializeComponent();
            this.loadButton.Enabled = false;
            fxViewWindow = new gfxViewWindow();
            settingForm = new SettingsForm(this);
        }

        List<Mod> modCollection = new List<Mod>();
        System.Drawing.Size initialFormSize;
        public gfxViewWindow fxViewWindow;
        SettingsForm settingForm;
        Dictionary<Control, System.Drawing.Size> initialSize = new Dictionary<Control, System.Drawing.Size>();
        Dictionary<Control, System.Drawing.Point> initialLocation = new Dictionary<Control, System.Drawing.Point>();
		List<String> filesToLoad = new List<string>();
		HashSet<String> _hFilesToLoad = new HashSet<string>();

        private void lockSettingsCheckBox_CheckedChanged(object sender, EventArgs e) {
            if (this.lockSettingsCheckBox.Checked) {
                this.gameDirectoryBox.Enabled = false;
                this.hoi4DirectorySelect.Enabled = false;
                this.addModButton.Enabled = false;
                this.removeModButton.Enabled = false;
                this.loadButton.Enabled = true;
            }
            else {
                this.gameDirectoryBox.Enabled = true;
                this.hoi4DirectorySelect.Enabled = true;
                this.addModButton.Enabled = true;
                this.removeModButton.Enabled = true;
                this.loadButton.Enabled = false;
            }
        }

        private void hoi4DirectorySelect_Click(object sender, EventArgs e) {

        }

        private void loadButton_Click(object sender, EventArgs e) {
            String directory = this.gameDirectoryBox.Text;
            loadButton.Enabled = false;
            loadButton.Text = "Loading...";

            Thread loadThread = new Thread(new ThreadStart(LoadFiles));

            // Begin loading.
            loadThread.Start();
            
            
        }

        private void addModButton_Click(object sender, EventArgs e) {
            OpenFileDialog open = new OpenFileDialog();
            open.ShowDialog();

            string path = open.FileName;

            if (!path.EndsWith(".mod")) {
                var result = MessageBox.Show("File: " + open.SafeFileName + " does not end with extension .mod" + Environment.NewLine + "Do you wish to continue?", "File Extension Warning", MessageBoxButtons.YesNo);
                if (result == DialogResult.No) { return;  }
            }


            Mod nMod = Mod.ParseModFile(path);



            outputBox.Text += nMod.ToString();



        }

        private void MenuForm_Load(object sender, EventArgs e) {
            fxViewWindow.Show();
            fxViewWindow.Hide();
            initialFormSize = this.Size;
            
            foreach (Control control in this.Controls) {
                this.initialSize[control] = control.Size;
                this.initialLocation[control] = control.Location;
            }
        }


        private System.Drawing.Bitmap BitmapFromWriteableBitmap(WriteableBitmap writeBmp) {
            System.Drawing.Bitmap bmp;
            using (MemoryStream outStream = new MemoryStream()) {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create((BitmapSource)writeBmp));
                enc.Save(outStream);
                bmp = new System.Drawing.Bitmap(outStream);
            }
            return bmp;
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {

        }

        private void clearOutputButton_Click(object sender, EventArgs e) {
            outputBox.Text = "";
        }


        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e) {

        }

        private void toolStripButton1_Click(object sender, EventArgs e) {
            String_Literal_View nView = new String_Literal_View();
            nView.Show();
        }

        private void focusTreeButton_Click(object sender, EventArgs e) {

        }

        private void LoadFiles() {

            

        }

        private void removeModButton_Click(object sender, EventArgs e) {
            OpenFileDialog open = new OpenFileDialog();
            open.ShowDialog();

            string path = open.FileName;

            Thread parseThread = new Thread(() => doGFXParse(path));
            parseThread.Start();



        }

        private void doGFXParse(String filePath) {
            GFXParserResult parserResult = GFXParser.ParseGFXFIle(filePath, true);
            string gfxBuild = "";
            if (parserResult.Success) {
                foreach (GFXSprite fXSprite in parserResult.Result) {
                    fXSprite.ConstructFilePath();
                    gfxBuild += fXSprite.Name + " - " + fXSprite.TextureFile + Environment.NewLine + "FPATH: " + fXSprite.FullTexturePath + Environment.NewLine;
                    

                }
                this.Invoke(new Action(() => outputBox.Text = gfxBuild));
            }
            else {

                this.Invoke(new Action(() => outputBox.Text = parserResult.FailureReason));

            }



        }


        static string build = "";
        private void recursiveTree(Token token) {
            if (token.Parent != null) {
                for (int  i = 0; i != token.Depth; i++) { build += "\t"; }
                switch (token.tokenType) {
                    case TokenType.Comment:
                        build += "Comment Token [" + token.Parent.Key + "]" + Environment.NewLine;
                        break;
                    case TokenType.KeyValue:
                        build += token.Key + " [" + token.Parent.Key + "]" + " = "+ token.Value + Environment.NewLine;
                        break;
                    default:
                        build += token.Key + " [" + token.Parent.Key + "]" + Environment.NewLine;
                        break;
                }
            }
            else {
                build += token.Key + Environment.NewLine;
            }
            foreach (Token child in token.Children) {
                recursiveTree(child);
            }


        }

        private void viewGFXTopbarButton_Click(object sender, EventArgs e) {
            fxViewWindow.Show();
            fxViewWindow.Enabled = true;
            foreach (KeyValuePair<String, GFXSprite> gfxSprite in GFXSprite.GFXSprites) {

                gfxSprite.Value.LoadTexture(fxViewWindow.viewGFXPanel1.GraphicsDevice);
            }

        }

        private void loadTexture() {
            
        }

        private void toolStripButton2_Click(object sender, EventArgs e) {
            this.settingForm.Show();
        }

        private void MenuForm_Resize(object sender, EventArgs e) {
            double widthScale = this.Size.Width / this.initialFormSize.Width;
            double heightScale = this.Size.Height / this.initialFormSize.Height;

            foreach (Control control in this.Controls) {
                System.Drawing.Size size = new System.Drawing.Size((int)(initialSize[control].Width * widthScale), (int)(initialSize[control].Height * heightScale));
                control.Size = size;

                System.Drawing.Point point = new System.Drawing.Point(
                    (int)(initialLocation[control].X * widthScale), (int)(initialLocation[control].Y * heightScale));
                control.Location = point;
            }

        }

        private void loadButtonFile_Click(object sender, EventArgs e) {
            OpenFileDialog open = new OpenFileDialog();
            open.ShowDialog();

            string path = open.FileName;

            if (path == "") {
                return;
            }

			if (_hFilesToLoad.Contains(path)) { return; }
			_hFilesToLoad.Add(path);
			ImportFile file = new ImportFile(open.SafeFileName, path);
			modBox.Items.Add(file);


        }

        private void MenuForm_FormClosed(object sender, FormClosedEventArgs e) {



        }

		private void loadFilesButton_Click(object sender, EventArgs e) {

			ImportFile[] importFiles = modBox.Items.OfType<ImportFile>().ToArray();

			if (importFiles.Length == 0) {
				new Thread(() => {
					MessageBox.Show("No imported files were found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}).Start();
				return;
			}

			Token[] tokenizedFiles = new Token[importFiles.Length];

			this.progressBar1.Maximum = importFiles.Length - 1;
			this.progressBar1.Minimum = 0;
			new Thread(() => {
				for (int i = 0; i != importFiles.Length; i++) {
					this.Invoke(new Action(()=> { this.progressBar1.Value = i; }));
					importFiles[i].RootToken = Tokenizer.TokenizeFile(importFiles[i].Path);

					importFiles[i].FindFileType();
					this.Invoke(new Action(() => { outputBox.Text += importFiles[i].ImportFileType + Environment.NewLine; }));
					switch (importFiles[i].ImportFileType) {
						case ImportFileType.Focus:
							FocusParseResult focusParseResult = FocusParser.ParseRootFocusToken(importFiles[i].RootToken);
							foreach (var foci in focusParseResult.focusDict) {
								if (Hoi4_Mod_Tools.Focus.loadedFocus.TryAddKeyValue(foci)) {
									WriteLineToOutputBox($"Focus loaded: {foci.Key}");
								}
								else {
									WriteLineToOutputBox($"Failed to load: {foci.Key}");
								}
								
							}
							break;
						case ImportFileType.Event:
							break;
						case ImportFileType.GFX:
							break;
					}

					
				}
			}).Start();

		}

		private void WriteLineToOutputBox(String str) {
			this.Invoke(new Action(() => {
				outputBox.Text += str + Environment.NewLine;
			}));


		}
	}
}

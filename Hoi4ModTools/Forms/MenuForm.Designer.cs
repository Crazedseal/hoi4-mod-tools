﻿using System;
using System.Windows.Forms;

namespace Hoi4_Mod_Tools {
    partial class MenuForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuForm));
			this.hoi4DirectorySelect = new System.Windows.Forms.Button();
			this.gameDirectoryBox = new System.Windows.Forms.TextBox();
			this.addModButton = new System.Windows.Forms.Button();
			this.lockSettingsCheckBox = new System.Windows.Forms.CheckBox();
			this.loadButton = new System.Windows.Forms.Button();
			this.statusBox = new System.Windows.Forms.ListBox();
			this.modBox = new System.Windows.Forms.ListBox();
			this.removeModButton = new System.Windows.Forms.Button();
			this.outputBox = new System.Windows.Forms.TextBox();
			this.clearOutputButton = new System.Windows.Forms.Button();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.viewGFXTopbarButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
			this.focusTreeButton = new System.Windows.Forms.Button();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.importFileButton = new System.Windows.Forms.Button();
			this.loadFilesButton = new System.Windows.Forms.Button();
			this.toolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// hoi4DirectorySelect
			// 
			this.hoi4DirectorySelect.Location = new System.Drawing.Point(12, 28);
			this.hoi4DirectorySelect.Name = "hoi4DirectorySelect";
			this.hoi4DirectorySelect.Size = new System.Drawing.Size(123, 29);
			this.hoi4DirectorySelect.TabIndex = 0;
			this.hoi4DirectorySelect.Text = "Game Directory Select";
			this.hoi4DirectorySelect.UseVisualStyleBackColor = true;
			this.hoi4DirectorySelect.Click += new System.EventHandler(this.hoi4DirectorySelect_Click);
			// 
			// gameDirectoryBox
			// 
			this.gameDirectoryBox.Location = new System.Drawing.Point(141, 33);
			this.gameDirectoryBox.Name = "gameDirectoryBox";
			this.gameDirectoryBox.Size = new System.Drawing.Size(454, 20);
			this.gameDirectoryBox.TabIndex = 1;
			// 
			// addModButton
			// 
			this.addModButton.Location = new System.Drawing.Point(12, 60);
			this.addModButton.Name = "addModButton";
			this.addModButton.Size = new System.Drawing.Size(69, 29);
			this.addModButton.TabIndex = 2;
			this.addModButton.Text = "Add Mod";
			this.addModButton.UseVisualStyleBackColor = true;
			this.addModButton.Click += new System.EventHandler(this.addModButton_Click);
			// 
			// lockSettingsCheckBox
			// 
			this.lockSettingsCheckBox.AutoSize = true;
			this.lockSettingsCheckBox.Location = new System.Drawing.Point(12, 95);
			this.lockSettingsCheckBox.Name = "lockSettingsCheckBox";
			this.lockSettingsCheckBox.Size = new System.Drawing.Size(89, 17);
			this.lockSettingsCheckBox.TabIndex = 3;
			this.lockSettingsCheckBox.Text = "Lock Options";
			this.lockSettingsCheckBox.UseVisualStyleBackColor = true;
			this.lockSettingsCheckBox.CheckedChanged += new System.EventHandler(this.lockSettingsCheckBox_CheckedChanged);
			// 
			// loadButton
			// 
			this.loadButton.Location = new System.Drawing.Point(12, 118);
			this.loadButton.Name = "loadButton";
			this.loadButton.Size = new System.Drawing.Size(180, 51);
			this.loadButton.TabIndex = 4;
			this.loadButton.Text = "Load";
			this.loadButton.UseVisualStyleBackColor = true;
			this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
			// 
			// statusBox
			// 
			this.statusBox.FormattingEnabled = true;
			this.statusBox.Location = new System.Drawing.Point(384, 60);
			this.statusBox.Name = "statusBox";
			this.statusBox.Size = new System.Drawing.Size(211, 108);
			this.statusBox.TabIndex = 5;
			// 
			// modBox
			// 
			this.modBox.DisplayMember = "Name";
			this.modBox.FormattingEnabled = true;
			this.modBox.Location = new System.Drawing.Point(198, 60);
			this.modBox.Name = "modBox";
			this.modBox.Size = new System.Drawing.Size(180, 108);
			this.modBox.TabIndex = 6;
			this.modBox.ValueMember = "Name";
			// 
			// removeModButton
			// 
			this.removeModButton.Location = new System.Drawing.Point(87, 60);
			this.removeModButton.Name = "removeModButton";
			this.removeModButton.Size = new System.Drawing.Size(105, 29);
			this.removeModButton.TabIndex = 7;
			this.removeModButton.Text = "Remove Mod";
			this.removeModButton.UseVisualStyleBackColor = true;
			this.removeModButton.Click += new System.EventHandler(this.removeModButton_Click);
			// 
			// outputBox
			// 
			this.outputBox.AcceptsReturn = true;
			this.outputBox.AcceptsTab = true;
			this.outputBox.Location = new System.Drawing.Point(12, 174);
			this.outputBox.Multiline = true;
			this.outputBox.Name = "outputBox";
			this.outputBox.ReadOnly = true;
			this.outputBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.outputBox.Size = new System.Drawing.Size(583, 222);
			this.outputBox.TabIndex = 8;
			this.outputBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// clearOutputButton
			// 
			this.clearOutputButton.Location = new System.Drawing.Point(601, 402);
			this.clearOutputButton.Name = "clearOutputButton";
			this.clearOutputButton.Size = new System.Drawing.Size(127, 29);
			this.clearOutputButton.TabIndex = 9;
			this.clearOutputButton.Text = "Clear";
			this.clearOutputButton.UseVisualStyleBackColor = true;
			this.clearOutputButton.Click += new System.EventHandler(this.clearOutputButton_Click);
			// 
			// toolStrip1
			// 
			this.toolStrip1.GripMargin = new System.Windows.Forms.Padding(10, 2, 2, 2);
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.viewGFXTopbarButton,
            this.toolStripButton2});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
			this.toolStrip1.Size = new System.Drawing.Size(740, 25);
			this.toolStrip1.TabIndex = 11;
			this.toolStrip1.Text = "aaaaa";
			this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
			// 
			// toolStripButton1
			// 
			this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
			this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton1.Name = "toolStripButton1";
			this.toolStripButton1.Size = new System.Drawing.Size(126, 22);
			this.toolStripButton1.Text = "View String Literals";
			this.toolStripButton1.ToolTipText = "View Strings";
			this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
			// 
			// viewGFXTopbarButton
			// 
			this.viewGFXTopbarButton.Image = ((System.Drawing.Image)(resources.GetObject("viewGFXTopbarButton.Image")));
			this.viewGFXTopbarButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.viewGFXTopbarButton.Name = "viewGFXTopbarButton";
			this.viewGFXTopbarButton.Size = new System.Drawing.Size(76, 22);
			this.viewGFXTopbarButton.Text = "View GFX";
			this.viewGFXTopbarButton.Click += new System.EventHandler(this.viewGFXTopbarButton_Click);
			// 
			// toolStripButton2
			// 
			this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
			this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton2.Name = "toolStripButton2";
			this.toolStripButton2.Size = new System.Drawing.Size(97, 22);
			this.toolStripButton2.Text = "View Settings";
			this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
			// 
			// focusTreeButton
			// 
			this.focusTreeButton.Enabled = false;
			this.focusTreeButton.Location = new System.Drawing.Point(601, 30);
			this.focusTreeButton.Name = "focusTreeButton";
			this.focusTreeButton.Size = new System.Drawing.Size(127, 29);
			this.focusTreeButton.TabIndex = 12;
			this.focusTreeButton.Text = "Focus Tree Editor";
			this.focusTreeButton.UseVisualStyleBackColor = true;
			this.focusTreeButton.Click += new System.EventHandler(this.focusTreeButton_Click);
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(12, 402);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(583, 29);
			this.progressBar1.TabIndex = 13;
			// 
			// importFileButton
			// 
			this.importFileButton.Location = new System.Drawing.Point(601, 332);
			this.importFileButton.Name = "importFileButton";
			this.importFileButton.Size = new System.Drawing.Size(127, 29);
			this.importFileButton.TabIndex = 14;
			this.importFileButton.Text = "Import File";
			this.importFileButton.UseVisualStyleBackColor = true;
			this.importFileButton.Click += new System.EventHandler(this.loadButtonFile_Click);
			// 
			// loadFilesButton
			// 
			this.loadFilesButton.Location = new System.Drawing.Point(601, 367);
			this.loadFilesButton.Name = "loadFilesButton";
			this.loadFilesButton.Size = new System.Drawing.Size(127, 29);
			this.loadFilesButton.TabIndex = 15;
			this.loadFilesButton.Text = "Load Files";
			this.loadFilesButton.UseVisualStyleBackColor = true;
			this.loadFilesButton.Click += new System.EventHandler(this.loadFilesButton_Click);
			// 
			// MenuForm
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
			this.ClientSize = new System.Drawing.Size(740, 441);
			this.Controls.Add(this.loadFilesButton);
			this.Controls.Add(this.importFileButton);
			this.Controls.Add(this.hoi4DirectorySelect);
			this.Controls.Add(this.focusTreeButton);
			this.Controls.Add(this.clearOutputButton);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.modBox);
			this.Controls.Add(this.gameDirectoryBox);
			this.Controls.Add(this.removeModButton);
			this.Controls.Add(this.addModButton);
			this.Controls.Add(this.statusBox);
			this.Controls.Add(this.lockSettingsCheckBox);
			this.Controls.Add(this.outputBox);
			this.Controls.Add(this.loadButton);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "MenuForm";
			this.Text = "Hearts of Iron IV Mod Tools";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MenuForm_FormClosed);
			this.Load += new System.EventHandler(this.MenuForm_Load);
			this.Resize += new System.EventHandler(this.MenuForm_Resize);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.Button hoi4DirectorySelect;
        private System.Windows.Forms.TextBox gameDirectoryBox;
        private System.Windows.Forms.Button addModButton;
        private System.Windows.Forms.CheckBox lockSettingsCheckBox;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.ListBox statusBox;
        private System.Windows.Forms.ListBox modBox;
        private System.Windows.Forms.Button removeModButton;
        public System.Windows.Forms.TextBox outputBox;
        private System.Windows.Forms.Button clearOutputButton;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Button focusTreeButton;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ToolStripButton viewGFXTopbarButton;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Button importFileButton;
		private Button loadFilesButton;
	}
}
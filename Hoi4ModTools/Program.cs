﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Hoi4_Mod_Tools {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        public static MenuForm mainMenuForm;

        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            mainMenuForm = new MenuForm();
            Application.Run(mainMenuForm);
        }
    }
}
